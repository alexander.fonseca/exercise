package co.com.pragma.imageservice.imageservice.entity;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "tbl_images")
public class Image implements Serializable {

    @Id
    private String id;

    private String content;

    private String type;

}

